/*
	Package status extends package google.golang.org/grpc/status
*/

package status

import (
	"github.com/golang/protobuf/proto"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/wrapperspb"
)

// Details list detail errors
type Details []string

// Status wrap on struct Status (google.golang.org/grpc/status)
type Status struct {
	status *status.Status
}

// New returns a Status representing c and msg.
func New(c codes.Code, msg string) *Status {
	return &Status{
		status: status.New(c, msg),
	}
}

// Newf returns New(c, fmt.Sprintf(format, a...)).
func Newf(c codes.Code, format string, a ...any) *Status {
	return &Status{
		status: status.Newf(c, format, a...),
	}
}

// WithDetails add details to answer
func (s *Status) WithDetails(details Details) error {
	var err error
	s.status, err = s.status.WithDetails(toMessage(details)...)
	return err
}

// Err returns an immutable error representing s; returns nil if s.Code() is OK.
func (s *Status) Err() error {
	return s.status.Err()
}

// Error returns an error representing c and msg.  If c is OK, returns nil.
func Error(c codes.Code, msg string) error {
	return status.Error(c, msg)
}

// Errorf returns Error(c, fmt.Sprintf(format, a...)).
func Errorf(c codes.Code, format string, a ...any) error {
	return status.Errorf(c, format, a...)
}

// ErrorWithDetails returns an error representing c and msg and datails
// If it was not possible to add details, then an error with a code and a message will simply be returned
func ErrorWithDetails(c codes.Code, msg string, details Details) error {
	s := status.New(c, msg)

	statusWithDetails, err := s.WithDetails(toMessage(details)...)
	if err != nil {
		return s.Err()
	}

	return statusWithDetails.Err()
}

// ErrorfWithDetails returns Error(c, fmt.Sprintf(format, a...)) and details.
// If it was not possible to add details, then an error with a code and a message will simply be returned
func ErrorfWithDetails(c codes.Code, format string, details Details, a ...any) error {
	s := status.Newf(c, format, a...)

	statusWithDetails, err := s.WithDetails(toMessage(details)...)
	if err != nil {
		return s.Err()
	}

	return statusWithDetails.Err()
}

// toMessage convert Details to proto.MessageV1
func toMessage(details Details) []proto.Message {
	messages := make([]proto.Message, 0, len(details))

	for _, d := range details {
		messages = append(messages, proto.MessageV1(wrapperspb.String(d)))
	}

	return messages
}
