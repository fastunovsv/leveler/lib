module gitlab.com/fastunovsv/leveler/lib

go 1.21.1

require (
	github.com/golang/protobuf v1.5.3
	google.golang.org/grpc v1.59.0
	google.golang.org/protobuf v1.31.0
)

require google.golang.org/genproto/googleapis/rpc v0.0.0-20230822172742-b8732ec3820d // indirect
